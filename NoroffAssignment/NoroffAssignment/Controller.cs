﻿using NoroffAssignment.Model;
using System;
using System.Collections.Generic;
using System.Linq;


namespace NoroffAssignment
{
    class Controller
    {
        public Controller()
        {
            Animal[] animalArray = new Animal[3]
            {
                new Animal("Tiger", "Smith", 10),
                new Animal("Dog", "Johnson", 20),
                new Animal("Cat", "Rabon", 15),
            };
            AnimalKingdom animalList = new AnimalKingdom(animalArray);
            foreach (Animal a in animalList)
            {
                Console.WriteLine($"Specie: {a.Species}\nName: {a.Name}\nAge: {a.Age} \n");
            }

            List<Animal> tmp = new List<Animal>();
            tmp.Add(new Animal("Tiger", "Smith", 10));
            tmp.Add(new Animal("Dog", "Johnson", 20));
            tmp.Add(new Animal("Cat", "Rabon", 15));

            IEnumerable<Animal> test = from a in tmp
                                where a.Age > 10
                                select a;

            foreach(Animal a in test)
            {
                Console.WriteLine($"Specie: {a.Species}\nName: {a.Name}\nAge: {a.Age} \n");
            }
        }
    }
}
