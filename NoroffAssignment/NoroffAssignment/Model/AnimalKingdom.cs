﻿using NoroffAssignment.Model.Enum;
using System.Collections;
using System.Linq;

namespace NoroffAssignment.Model
{
    class AnimalKingdom : IEnumerable
    {
        private Animal[] _animal;
        public AnimalKingdom(Animal[] aArray)
        {
            _animal = new Animal[aArray.Length];

            for (int i = 0; i < aArray.Length; i++)
            {
                _animal[i] = aArray[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public AnimalKingdomEnum GetEnumerator()
        {
            return new AnimalKingdomEnum(_animal);
        }
    }
}
