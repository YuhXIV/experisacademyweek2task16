﻿using System;
using System.Collections;

namespace NoroffAssignment.Model.Enum
{
    class AnimalKingdomEnum : IEnumerator
    {
        public Animal[] _animal;
        int position = -1;

        public AnimalKingdomEnum(Animal[] list)
        {
            _animal = list;
        }

        public bool MoveNext()
        {
            position++;
            return (position < _animal.Length);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Animal Current
        {
            get
            {
                try
                {
                    return _animal[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}
