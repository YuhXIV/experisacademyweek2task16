﻿using NoroffAssignment.Model.Enum;
using System.Collections;

namespace NoroffAssignment.Model
{
    class Animal
    {   
        public Animal(string species,  string name, int age)
        {
            Name = name;
            Age = age;
            Species = species;
        }

        public Animal(string species, string name, int age, double weight, double height)
        {
            Name = name;
            Age = age;
            Species = species;
            Weight = weight;
            Height = height;
        }

        public string Roar { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
        public string Species { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }

        public bool DiedOfAge(int lifespan)
        {
            if(Age > lifespan)
            {
                return true;
            }
            return false;
        }

        public bool Matching (string name)
        {
            if (this.Name.ToLower().Contains(name.ToLower())) return true;
            return false;
        }
    }
}
